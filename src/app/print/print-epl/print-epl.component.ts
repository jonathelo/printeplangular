import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-print-epl',
  templateUrl: './print-epl.component.html',
  styleUrls: ['./print-epl.component.css']
})
export class PrintEPLComponent implements OnInit, OnChanges {

  @Input() printEPLE: string = '';

  public title = 'printer';
  public selectedPrinter: any;
  public printers: string[];
  public isDefaultPrinterSelected: Boolean = true;

  constructor() {
    //webSocket print EPL
    JSPM.JSPrintManager.auto_reconnect = true;
    JSPM.JSPrintManager.start();
    JSPM.JSPrintManager.WS.onStatusChanged = () => {
      if (this.jspmWSStatus()) {
        // get client installed printers
        JSPM.JSPrintManager.getPrinters().then((myPrinters: string[]) => {
          this.printers = myPrinters;
          console.log(this.printers);
        });

      }
    };
  }

  ngOnInit(): void {
    console.log('ngOnInit');
  }

  ngOnChanges(): void {
    console.log('ngOncChange');
    console.log(this.printEPLE);
    if (this.printEPLE !== '') {
      this.doPrint(this.printEPLE);
    }
  }

  // Check JSPM WebSocket status
  jspmWSStatus() {
    if (JSPM.JSPrintManager.websocket_status === JSPM.WSStatus.Open) {
      return true;
    } else if (JSPM.JSPrintManager.websocket_status === JSPM.WSStatus.Closed) {
      //alerta personalizada sin no detecta el JSPM activado segun plantilla
      //alert('JSPrintManager (JSPM) is not installed or not running! Download JSPM Client App from https://neodynamic.com/downloads/jspm');
      alert('JSPrintManager (JSPM) no esta instalado o no esta corriendo en el equipo! Descargue JSPM Client App de https://neodynamic.com/downloads/jspm');
      return false;
    } else if (JSPM.JSPrintManager.websocket_status === JSPM.WSStatus.Blocked) {
      //alerta personalizada para detectar sitios bloqueados
      //alert('JSPM has blacklisted this website!');
      alert('JSPM tiene en bloqueado este sitio Web');
      return false;
    }
  }


  // Do printing...
  async doPrint(cmdEPL: string) {
    //console.log(this.selectedPrinter);
    if (this.jspmWSStatus()) {
      // Create a ClientPrintJob
      const cpj = new JSPM.ClientPrintJob();
      // Set Printer type (Refer to the help, there many of them!)
      console.log(this.isDefaultPrinterSelected);

      //en el caso de no se detecten impresoras inicio proceso de carga de impresoras disponibles
      if (this.printers.length === 0) {
        //espera de carga de impresoras
        await JSPM.JSPrintManager.getPrinters().then((myPrinters: string[]) => {
          this.printers = myPrinters;
          console.log(this.printers);
        });
      }
      
      if (this.isDefaultPrinterSelected) {
        cpj.clientPrinter = new JSPM.DefaultPrinter();
      } else {
        cpj.clientPrinter = new JSPM.InstalledPrinter(this.selectedPrinter);
      }

      //Set content to print...
      //Create Zebra EPL commands for sample label
      var lineFeed = "\x0A";//comando se salto de @linea o nuevo comando segun documentacion JSPM

      var cmds = '';//variable base para todo el comando a ser impreso

      cmds += lineFeed;//primera lina previa a culquier comando preexistente

      cmds += cmdEPL.replace(new RegExp("\n", "g"), lineFeed);//comandos a ser impresos segun recepcion de valores

      cmds += lineFeed;// linea final de comandos 

      //enviamos comando a ser impresos pero reeplazamos el salto de linea por lineFeed en todos los comandos recibidos
      cpj.printerCommands = cmds;

      console.log(cmds);
      //Send print job to printer!
      cpj.sendToClient();

    }
  }
}

# Print Zebra EPL
(gestion y control de impresora ZEBRA con comandos EPL)

## copy all
(copiar todo el contenido situacio en la siguiente ruta del proyecto)
```
├── src/
│   ├── app/
        ├── print/(all)
```

## add scripts in angular.json
(añadir los script scontenidos dentro de print->script en angular.json)

```
 "scripts": [
            "src/app/print/print-epl/scripts/JSPrintManager"
            ,"src/app/print/print-epl/scripts/zip"
            ,"src/app/print/print-epl/scripts/zip-ext"
            ,"src/app/print/print-epl/scripts/deflate"
            ] 
```

## import in modules externals
```
(para su importacion en modulos externos se debe enviar el valor \[printEPL\] para la respectiva impresion)
<app-print-epl [printEPLE]="printEPLE" *ngIf="true"></app-print-epl>
```

## references
* [JSPrintManager v3.0.6](https://neodynamic.com/products/printing/js-print-manager)
* [zip](https://github.com/gildas-lormeau/zip.js)
* [zip-ext](https://github.com/gildas-lormeau/zip.js)
* [zip-ext](https://github.com/gildas-lormeau/zip.js)


## Creator
**Jonathan Reyes 2021**
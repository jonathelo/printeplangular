import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrintRoutingModule } from './print-routing.module';
import { PrintEPLComponent } from './print-epl/print-epl.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [PrintEPLComponent],
  imports: [
    CommonModule,
    PrintRoutingModule
    ,FormsModule 
  ],
  exports:[PrintEPLComponent]
})
export class PrintModule { }

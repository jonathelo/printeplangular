import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrls: ['./test1.component.css']
})
export class Test1Component implements OnInit {

  public textPrint: string = "";
  public printEPLE:string="";

  constructor() { }

  ngOnInit(): void {
  }


  imprimir() {
    console.log(this.textPrint);
    this.printEPLE=this.textPrint;
    //this.printEPLE="jonathan \n reyes \n tomala \n";
    console.log(this.printEPLE)
  }

}

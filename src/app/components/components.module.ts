import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { Test1Component } from './test1/test1.component';
import { PrintModule } from '../print/print.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [Test1Component],
  imports: [
    CommonModule,
    ComponentsRoutingModule
    ,PrintModule
    ,FormsModule
  ]
  ,exports:[Test1Component]
})
export class ComponentsModule { }
